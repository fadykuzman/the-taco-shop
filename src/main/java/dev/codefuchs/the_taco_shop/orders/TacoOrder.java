package dev.codefuchs.the_taco_shop.orders;

import dev.codefuchs.the_taco_shop.tacos.Taco;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TacoOrder {
    private Delivery delivery;
    private CreditCard creditCard;
    private List<Taco> tacos = new ArrayList<>();

    private void addTaco(Taco taco) {
        tacos.add(taco);
    }
}
