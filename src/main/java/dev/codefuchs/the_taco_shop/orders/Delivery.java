package dev.codefuchs.the_taco_shop.orders;

import lombok.Data;

@Data
public class Delivery {
    private String name;
    private String street;
    private String city;
    private String state;
    private String zip;
}
