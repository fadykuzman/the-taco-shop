package dev.codefuchs.the_taco_shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheTacoShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheTacoShopApplication.class, args);
    }

}
