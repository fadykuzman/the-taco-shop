package dev.codefuchs.the_taco_shop.tacos;

import dev.codefuchs.the_taco_shop.ingredients.Ingredient;
import lombok.Data;

import java.util.List;

@Data
public class Taco {
    private String name;
    private List<Ingredient> ingredients;
}
